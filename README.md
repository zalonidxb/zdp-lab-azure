# Deploy Hortonworks Sandbox infrastrucutre for ZDP Demos/POC on Azure

This script assumes you are running it from Azure Cloud Shell.
In order to access Azure Cloud shell you need an active azure subcription 
more information can you found in this video https://www.youtube.com/watch?v=RhnZ4lJgEnU 
Use it in bash mode

### Pre-Requisites 
You would need to create an sshkey on Azure CLI and send it to your contact at Zalnoi
Steps to create ssh key are given below and are only required once. If you are the Zaloni contact person please copy the 5 steps below and send it to your client/partner. 

1) Login to Azure Cloud Shell
2) ssh-keygen -q -f ~/.ssh/id_rsa -N ""
3) cat  ~/.ssh/id_rsa.pub
4) Copy the text generate and send it to your contact at Zaloni or any of below 
     gchakravarti@zaloni.com 
     fzafar@zaloni.com
     ddas@zaloni.com 
5) Wait for Email confirmation that your ssh key is added to our systems 

### Usage of Repo after ssh key is added by Zaloni team to secure valut 

1) Login to Azure Cloud Shell 
2) cd clouddrive 
3) git clone git@zalonisvn.git.beanstalkapp.com:/zalonisvn/cd-zdp-azure-hdp-lab.git
4) vi inventory/hdp-sandbox/group_vars/all/vars.yml 
   4.1) You can change the variable hdpvm_resource_group: zdp2050demo  to a suitabel unique name 
   4.2) You may want to choose a better VM size using variable hdpvm_machine_size: Standard_DS13-4_v2 
4) ./create-zdplab-ssh.sh

## Conclusions 
This will create a Hortonworks Sandbox-Host with a Hortron Works Docker
Enable SSH Key based authentication 
Get ZDPRPMs from a remote server 
Get ZDP Ansible Installer from repo 
Install ZDP in a fully automated manner 

## Authors 
Faisal Zafar 
Devayon Das 
Jitul Nath 



